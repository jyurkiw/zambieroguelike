/*
 * Engine.h
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#ifndef MODULES_ENGINE_H_
#define MODULES_ENGINE_H_

namespace zrgl {

	template<class S, class T>
	class Engine {
	public:
		S *dataStore;

		Engine(void)
		{
			dataStore = 0;
		}

		virtual ~Engine(void)
		{

		}

		// Draw the level to the screen.
		virtual void draw(void) = 0;
	};

} /* namespace zrgl */

#endif /* MODULES_ENGINE_H_ */
