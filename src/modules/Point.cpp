/*
 * Point.cpp
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#include "Point.h"

namespace zrgl {

	Point::Point(void) {
		x = 0;
		y = 0;
	}

	Point::Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

} /* namespace zrgl */
