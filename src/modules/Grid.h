/*
 * Grid.h
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#ifndef MODULES_GRID_H_
#define MODULES_GRID_H_

#include "Point.h"
#include <vector>

namespace zrgl {

	/**
	 * A simple multi-dimensional array of objects or primitive data types
	 * with basic support for retrieval and manipulation.
	 */
	template<class T>
	class Grid {
	public:
		int height;
		int width;

		// Default cell value.
		T defaultTVal;

		/**
		 * Initialize a grid with a given height, width, and default TValue.
		 */
		Grid(int height, int width, T defaultTVal)
		{
			this->height = height;
			this->width = width;
			this->defaultTVal = defaultTVal;

			data.resize(height);
			for(int i = 0; i < height; i++)
			{
				data[i].resize(width);
				data[i].assign(width, defaultTVal);
			}
		}

		/**
		 * Set every cell in the grid to its default TValue.
		 */
		void clear(void)
		{
			for(int x = 0; x < width; x++)
				for (int y = 0; y < height; y++)
					data[x][y] = this->defaultTVal;
		}

		/**
		 * Get the TValue array at position x.
		 * X = x-axis.
		 * X[y] = x-y point.
		 */
		std::vector<T> operator[](const int x)
		{
			return data[x];
		}

		/**
		 * Get the TValue at point p.
		 */
		T operator[](const Point p)
		{
			return data[p.x][p.y];
		}

		T get(int x, int y)
		{
			return data[x][y];
		}

		T get (const Point p)
		{
			return data[p.x][p.y];
		}

		void set(int x, int y, T val)
		{
			data[x][y] = val;
		}

		void set (const Point p, T val)
		{
			data[p.x][p.y] = val;
		}

	private:
		std::vector<std::vector<T> > data;
	};

} /* namespace zrgl */

#endif /* MODULES_GRID_H_ */
