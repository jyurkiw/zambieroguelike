/*
 * ActorDrawSystem.h
 *
 *  Created on: Aug 20, 2015
 *      Author: master
 */

#ifndef SYSTEMS_ACTORDRAWSYSTEM_H_
#define SYSTEMS_ACTORDRAWSYSTEM_H_

#include <EntityProcessingSystem.h>
#include <ComponentMapper.h>
#include <Entity.h>

#include "../components/DrawComponent.h"
#include "../components/PositionComponent.h"
#include "../modules/Grid.h"

namespace zrgl {

	template<class T>
	class ActorDrawSystem : public artemis::EntityProcessingSystem {
	public:
		ActorDrawSystem( Grid<T>* drawSurface )
		{
			addComponentType<PositionComponent>();
			addComponentType<DrawComponent<char> >();

			this->drawSurface = drawSurface;
		}

		~ActorDrawSystem()
		{
			drawSurface = 0;
		}

		virtual void initialize( void )
		{
			positionMapper.init(*world);
			drawMapper.init(*world);
		}

		virtual void processEntity(artemis::Entity& e)
		{
			drawSurface->set(positionMapper.get(e)->location, drawMapper.get(e)->drawTVal);
		}

	private:
		artemis::ComponentMapper<PositionComponent> positionMapper;
		artemis::ComponentMapper<DrawComponent<char> > drawMapper;

		Grid<T>* drawSurface;
	};

} /* namespace zrgl */

#endif /* SYSTEMS_ACTORDRAWSYSTEM_H_ */
