/*
 * Point.h
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#ifndef MODULES_POINT_H_
#define MODULES_POINT_H_

namespace zrgl {

	class Point {
	public:
		int x;
		int y;

		Point(void);
		Point(int, int);
	};

} /* namespace zrgl */

#endif /* MODULES_POINT_H_ */
