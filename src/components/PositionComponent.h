/*
 * positionComponent.h
 *
 *  Created on: Aug 20, 2015
 *      Author: master
 */

#ifndef COMPONENTS_POSITIONCOMPONENT_H_
#define COMPONENTS_POSITIONCOMPONENT_H_

#include <Component.h>

#include "../modules/Point.h"

namespace zrgl {

	class PositionComponent : public artemis::Component {
	public:
		Point location;

		PositionComponent(int, int);
	};

} /* namespace zrgl */

#endif /* COMPONENTS_POSITIONCOMPONENT_H_ */
