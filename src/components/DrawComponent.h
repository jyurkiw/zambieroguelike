/*
 * drawComponent.h
 *
 *  Created on: Aug 20, 2015
 *      Author: master
 */

#ifndef COMPONENTS_DRAWCOMPONENT_H_
#define COMPONENTS_DRAWCOMPONENT_H_

#include <Component.h>

namespace zrgl {

	template<class T>
	class DrawComponent: public artemis::Component {
	public:
		T drawTVal;

		DrawComponent(T drawTVal)
		{
			this->drawTVal = drawTVal;
		}
	};

} /* namespace zrgl */

#endif /* COMPONENTS_DRAWCOMPONENT_H_ */
