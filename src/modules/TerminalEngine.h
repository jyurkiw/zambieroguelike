/*
 * TerminalEngine.h
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#ifndef MODULES_TERMINALENGINE_H_
#define MODULES_TERMINALENGINE_H_

#include <string>

#include "Engine.h"
#include "Grid.h"

namespace zrgl {

	class TerminalEngine: public Engine<Grid<char>, char> {
	public:
		TerminalEngine(int, int);
		virtual ~TerminalEngine(void);

		virtual void draw(void);
		void loadTestMap(std::string);
	};

} /* namespace zrgl */

#endif /* MODULES_TERMINALENGINE_H_ */
