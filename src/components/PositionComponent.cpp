/*
 * positionComponent.cpp
 *
 *  Created on: Aug 20, 2015
 *      Author: master
 */

#include "PositionComponent.h"
#include "../modules/Point.h"

namespace zrgl {

	PositionComponent::PositionComponent(int x, int y) {
		this->location.x = x;
		this->location.y = y;
	}

} /* namespace zrgl */
