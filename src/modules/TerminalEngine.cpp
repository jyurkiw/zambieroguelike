/*
 * TerminalEngine.cpp
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "TerminalEngine.h"
#include "Grid.h"

namespace zrgl {

	TerminalEngine::TerminalEngine(int height, int width) {
		dataStore = new Grid<char>(height, width, ' ');
	}

	TerminalEngine::~TerminalEngine()
	{
		delete dataStore;
	}

	/*
	 * Draw the grid to the terminal.
	 */
	void TerminalEngine::draw(void)
	{
		// Clear the terminal.
		std::cout << "\033[2J\033[1;1H" << std::endl;

		// Print out the contents of the grid.
		for (int y = dataStore->height - 1; y >= 0; y--)
		{
			for (int x = 0; x < dataStore->width; x++)
				std::cout << (*dataStore)[x][y];
			std::cout << '\n';
		}

		// Flush the buffer.
		std::cout << std::endl;
	}

	/*
	 * Load a map level from the file with the passed name
	 * into the Grid dataStore.
	 */
	void TerminalEngine::loadTestMap(std::string filename)
	{
		std::vector<std::string> lines;
		std::string line;
		std::ifstream level(filename.c_str());

		if (level.is_open())
			while (getline(level, line))
				lines.push_back(line);
		level.close();
		std::reverse(lines.begin(), lines.end());

		for (unsigned y = 0; y < lines.size(); y++)
		{
			for (unsigned int x = 0; x < lines[y].size(); x++)
			{
				dataStore->set(x, y, lines[y][x]);
			}
		}

	}

} /* namespace zrgl */
