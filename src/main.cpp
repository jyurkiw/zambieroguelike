/*
 * main.cpp
 *
 *  Created on: Aug 16, 2015
 *      Author: master
 */

#include <iostream>

#include <Artemis.h>

#include "components/DrawComponent.h"
#include "components/DrawComponent.h"
#include "modules/TerminalEngine.h"
#include "modules/Grid.h"

#include "components/PositionComponent.h"
#include "systems/ActorDrawSystem.h"

using namespace artemis;
using namespace zrgl;

int main (int argc, const char** argv)
{
	TerminalEngine te(20, 20);
	te.loadTestMap("ZRL_Level.txt");

	World world;
	SystemManager* systemManager = world.getSystemManager();
	ActorDrawSystem<char>* drawSystem = (ActorDrawSystem<char>*)systemManager->setSystem(new ActorDrawSystem<char>(te.dataStore));
	EntityManager* em = world.getEntityManager();

	systemManager->initializeAll();

	// create the player
	Entity& player = em->create();
	player.addComponent(new PositionComponent(9, 1));
	player.addComponent(new DrawComponent<char>('@'));
	player.refresh();

	std::string input;
	while(input != "exit")
	{
		world.loopStart();
		world.setDelta(0.0016f);
		drawSystem->process();
		te.draw();

		std::cout << "> ";
		std::cin >> input;
	}

	return 0;
}


